import React, { useState } from 'react';

import axios from 'axios';

const Form = (props) => {

    // State to hold entirety of form information
    const [formInfo, setFormInfo] = useState({
        deadline: '',
        goals: '',
        firstImp: '',
        contact: '',
        frequency: 'Daily',
        medium: '',
        budget: 0,
        img: ''
    })

    // Error flag states for each input to display specific error messages
    const [deadlineError, setDeadlineError] = useState(false);
    const [goalsError, setGoalsError] = useState(false);
    const [firstImpError, setFirstImpError] = useState(false);
    const [contactError, setContactError] = useState(false);
    const [mediumError, setMediumError] = useState(false);
    const [budgetError, setBudgetError] = useState(false);
    const [imgError, setImgError] = useState(false);

    // Function to handle change of all input fields and setState of formInfo
    const handleChange = (event) => {
        const { name, value } = event.target;

        setFormInfo((prev) => {
            return {
                ...prev,
                [name]: value
            }
        });
    }

    // Function to call when checking off a box from check group. Unchecks all other boxes then updates the medium state of formInfo
    const handleCheck = (event) => {
        let checkedData = event.target.name;

        // Uncheck the rest of the boxes
        const checkboxes = document.querySelectorAll('input[type="checkbox"]');
        checkboxes.forEach((box) => {
            if (box.name !== checkedData) {
                box.checked = false;
            }
            // Reset state back to empty if setting to unchecked
            else {
                if (!box.checked) {
                    checkedData = "";
                }
            }
        });

        // Update state of formInfo
        setFormInfo((prev) => {
            return {
                ...prev,
                medium: checkedData
            }
        });
    }

    // Function to check if the input string only contains special characters
    const onlySpecials = (inputString) => {
        let pattern = /^[^a-zA-Z]+$/;
        return (pattern.test(inputString));
    }

    // Function to validate all form inputs before submitting. Returns a boolean value to determine if valid to submit or not.
    const validateForm = () => {

        // If stays true throughout entire function, then entire form is valid
        let success = true;

        // Validate in reverse order so scrollIntoView() prioritizes highest input section
        
        // Validate image input
        if (formInfo.img === "") {
            success = false;
            setImgError(true);
            document.getElementById("img").scrollIntoView();
        }
        else {
            // Validate correct file extensions
            let extension = formInfo.img.substring(formInfo.img.lastIndexOf(".") + 1).toLowerCase();
            if (extension !== "png" && extension !== "jpeg" && extension !== "bmp" && extension !== "gif" && extension !== "jpg") {
                success = false;
                setImgError(true);
                document.getElementById("img").scrollIntoView();
            }
            else {
                setImgError(false);
            }
        }

        // Validate Budget input
        if (formInfo.budget <= 0) {
            success = false;
            setBudgetError(true);
            document.getElementById("budget").scrollIntoView();
        }
        else {
            setBudgetError(false);
        }

        // Validate Contact Medium input
        if (formInfo.medium === "") {
            success = false;
            setMediumError(true);
            document.getElementById("medium").scrollIntoView();
        }
        else {
            setMediumError(false);
        }

        // Validate Contact input
        if (formInfo.contact === "" || onlySpecials(formInfo.contact)) {
            success = false;
            setContactError(true);
            document.getElementById("contact").scrollIntoView();
        }
        else {
            setContactError(false);
        }

        // Validate First Impressions input
        if (formInfo.firstImp === "" || onlySpecials(formInfo.firstImp)) {
            success = false;
            setFirstImpError(true);
            document.getElementById("firstImp").scrollIntoView();
        }
        else {
            setFirstImpError(false);
        }

        // Validate Goals/Requirements input
        if (formInfo.goals === "" || onlySpecials(formInfo.goals)) {
            success = false;
            setGoalsError(true);
            document.getElementById("goals").scrollIntoView();
        }
        else {
            setGoalsError(false);
        }

        // Validate Date input
        const maxLength = 10; // Max length of the string assuming MM/DD/YYYY
        if (formInfo.deadline === "" || formInfo.deadline.length > maxLength) {
            success = false;
            setDeadlineError(true);
            document.getElementById("deadline").scrollIntoView();
        }
        else {
            setDeadlineError(false);
        }

        // If no errors, can successfully submit
        if (success) {
            return true;
        }
        else {
            return false;
        }
    }

    // Function to call when submitting form. First checks for required fields and validates inputs
    const handleSubmit = () => {
        if (validateForm()) {
            // Valid input, so now make post request to database
            axios.post('http://localhost:3001/forms/add', formInfo)
                .then(() => {
                    console.log("Form Submission Successful");

                    // Scroll Back to top
                    document.body.scrollIntoView();

                    // Change phases in App.js
                    props.onSubmit();
                })
                .catch(err => console.log(err));
        }
    }

    return (
        <div className="card-container">
            <p id="deadline" className="card-title card-subtitle">When is our deadline for this landing page?</p>
            {deadlineError && <p className="card-text error-text">Please select a valid date.</p>}
            <div className="input-container">
                <input 
                    type="date"
                    name="deadline"
                    value={formInfo.deadline}
                    onChange={handleChange}
                />
            </div>

            <p id="goals" className="card-title card-subtitle">What goal(s) does your company hope to achieve through this landing page? What information is required to be displayed?</p>
            {goalsError && <p className="card-text error-text">Please use alphabetical characters and do not leave this field empty.</p>}
            <div className="input-container">
                <textarea
                    spellCheck="true"
                    wrap="hard"
                    className="input-text input"
                    rows="5"
                    placeholder="Gain more event awareness and acquire more attendees. Available dates and first 3 tiers of ticket prices."
                    name="goals"
                    value={formInfo.goals}
                    onChange={handleChange}
                />
            </div>

            <p id="firstImp" className="card-title card-subtitle">What word(s) would you use to describe the ideal first impression(s) this landing page should give?</p>
            {firstImpError && <p className="card-text error-text">Please use alphabetical characters and do not leave this field empty.</p>}
            <div className="input-container">
                <input 
                    type="text"
                    placeholder="Inviting, Engaging, Charming"
                    name="firstImp"
                    value={formInfo.firstImp}
                    onChange={handleChange}
                />
            </div>

            <p id="contact" className="card-title card-subtitle">Who is the point of contact?</p>
            {contactError && <p className="card-text error-text">Please use alphabetical characters and do not leave this field empty.</p>}
            <div className="input-container">
                <input 
                    type="text"
                    placeholder="Firstname Lastname"
                    name="contact"
                    value={formInfo.contact}
                    onChange={handleChange}
                />
            </div>

            <p className="card-title card-subtitle">How frequently would they like to receive progress reports?</p>
            <div className="input-container">
                <select
                    name="frequency"
                    value={formInfo.frequency}
                    onChange={handleChange}
                >
                    <option>Daily</option>
                    <option>Weekly</option>
                    <option>Every 2 Weeks</option>
                    <option>Every 3 Weeks</option>
                    <option>Monthly</option>
                    <option>Every 2 Months</option>
                </select>
            </div>

            <p id="medium" className="card-title card-subtitle">Through what medium would they like to receive progress reports?</p>
            {mediumError && <p className="card-text error-text">Please select your preferred contact method.</p>}
            <div className="input-container">
                <div className="form-check">
                    <input 
                        className="form-check-input" 
                        type="checkbox"
                        name="Virtual Meeting"
                        onClick={handleCheck}
                    />
                    <label className="form-check-label">
                        Virtual Meeting
                    </label>
                </div>

                <div className="form-check">
                    <input 
                        className="form-check-input" 
                        type="checkbox"
                        name="E-Mail"
                        onClick={handleCheck}
                    />
                    <label className="form-check-label">
                        E-Mail
                    </label>
                </div>

                <div className="form-check">
                    <input 
                        className="form-check-input" 
                        type="checkbox" 
                        name="Phone Call"
                        onClick={handleCheck}
                    />
                    <label className="form-check-label">
                        Phone Call
                    </label>
                </div>

                <div className="form-check">
                    <input 
                        className="form-check-input" 
                        type="checkbox"
                        name="Text Message"
                        onClick={handleCheck}
                    />
                    <label className="form-check-label">
                        Text Message
                    </label>
                </div>

                <div className="form-check">
                    <input 
                        className="form-check-input" 
                        type="checkbox"
                        name="In-Person Meeting"
                        onClick={handleCheck}
                    />
                    <label className="form-check-label">
                        In-Person Meeting
                    </label>
                </div>
            </div>

            <p id="budget" className="card-title card-subtitle">What is your approximate budget range for this landing page (in $)?</p>
            {budgetError && <p className="card-text error-text">Please input a numeric price value greater than zero.</p>}
            <div className="input-container">
                <input 
                    type="number"
                    placeholder="0.00"
                    min="0.00"
                    step="0.01"
                    name="budget"
                    value={formInfo.budget}
                    onChange={handleChange}
                />
            </div>

            <p id="img" className="card-title card-subtitle">Please include an image of this year's convention mascot!</p>
            {imgError && <p className="card-text error-text">Please upload a valid image file.</p>}
            <div className="input-container">
                <input 
                    type="file"
                    accept="image/png,image/jpeg,image/bmp,image/gif,image/jpg"
                    name="img"
                    value={formInfo.img}
                    onChange={handleChange}
                />
            </div>

            <button className="form-button" onClick={handleSubmit}>
                Submit
            </button>
        </div>
    )
};

export default Form;