import React, { useState } from 'react';

const PastForm = (props) => {

    // To show and hide this previous submission
    const [hide, setHide] = useState(true);

    const handleClick = () => {
        setHide((prev) => !prev);
    }

    return (
        <div className="card-container card-container-past">
            <div className="input-container input-container-past">
                <p className="card-title card-subtitle card-subtitle-past">{props.timestamp}</p>
                <button className="form-button form-button-past" onClick={handleClick}>
                    {hide ? "Show" : "Hide"}
                </button>
                {
                    !hide &&
                    <div>
                        <strong className="card-text card-text-past">When is our deadline for this landing page?</strong>
                        <p className="card-text card-text-past">{props.deadline}</p>

                        <strong className="card-text card-text-past">What goal(s) does your company hope to achieve through this landing page?</strong>
                        <p className="card-text card-text-past">{props.goals}</p>

                        <strong className="card-text card-text-past">What word(s) would you use to describe the ideal first impression(s) the landing page should give?</strong>
                        <p className="card-text card-text-past">{props.firstImp}</p>

                        <strong className="card-text card-text-past">Who is the point of contact?</strong>
                        <p className="card-text card-text-past">{props.contact}</p>

                        <strong className="card-text card-text-past">How frequently would they like to receive progress reports?</strong>
                        <p className="card-text card-text-past">{props.frequency}</p>

                        <strong className="card-text card-text-past">Through what medium would they like to receive progress reports?</strong>
                        <p className="card-text card-text-past">{props.medium}</p>

                        <strong className="card-text card-text-past">What is your approximate budget range for this landing page?</strong>
                        <p className="card-text card-text-past">{props.budget}</p>

                        <strong className="card-text card-text-past">Please include an image of this year's convention mascot!</strong>
                        <p className="card-text card-text-past">{props.img}</p>
                    </div>
                }
            </div>
        </div>
    )
};

export default PastForm;