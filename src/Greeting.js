import React from 'react';

const Greeting = (props) => {

    // Change phase in App back to 1
    const handleAgain = () => {
        props.onReset();
    }

    return (
        <div>
            {
                props.phase === 1 ? 
                <div className="card-container">
                    <p className="card-title">Hello!</p>
                    <p className="card-text">
                        Thank you again for allowing me to create the landing page for your upcoming video game conference!
                        I'd like to ask some questions to get a deeper understanding of your vision and ensure I meet your expectations.
                        Please fill out all fields below:
                    </p>
                </div> :
                <div className="card-container">
                    <p className="card-title">Thank you for your response!</p>
                    <p className="card-text">
                        I will be in close contact with you for progress updates! In the meantime, you can view this and any past submissions below:
                    </p>
                    <button className='form-button' onClick={handleAgain}>
                        Submit Another
                    </button>
                </div>
            }
        </div>
    )
}

export default Greeting;