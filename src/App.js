import React, { useState, useEffect } from 'react';

import axios from 'axios';

import Greeting from './Greeting';
import Form from './Form';
import PastForm from './PastForm';

function App() {

  // Phase 1 = Blank form to fill out and submit
  // Phase 2 = Successful form submission
  const [phase, setPhase] = useState(1);

  // Container state for past submissions
  const [prevForms, setPrevForms] = useState([]);

  // Functions to change phases
  const handlePhaseOne = () => {
    setPhase(1);
  }

  const handlePhaseTwo = () => {
    setPhase(2);
  }

  // Grab and update from database upon phase change
  useEffect(() => {
    // Grab from database
    axios.get('http://localhost:3001/forms/')
      .then(res => {
        setPrevForms((prev) => res.data);
      })
      .catch(err => console.log(err));
  }, [phase])

  return (
    <div>
      <p className="title">Product Questionnaire</p>
      <Greeting phase={phase} onReset={handlePhaseOne}/>
      {phase === 1 && <Form onSubmit={handlePhaseTwo} />}
      <p className="title">Past Submissions</p>
      {
        prevForms.map((form) => {
          return (
            <PastForm
              key={form._id}
              id={form._id}
              timestamp={form.timestamp}
              deadline={form.deadline}
              goals={form.goals}
              firstImp={form.firstImp}
              contact={form.contact}
              frequency={form.frequency}
              medium={form.medium}
              budget={form.budget}
              img={form.img}
            />
          )
        })
      }
    </div>
  );
}

export default App;
