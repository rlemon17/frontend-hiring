const mongoose = require('mongoose');

const formSchema = new mongoose.Schema({
    timestamp: {type: String},
    deadline: {type: String},
    goals: {type: String},
    firstImp: {type: String},
    contact: {type: String},
    frequency: {type: String},
    medium: {type: String},
    budget: {type: Number},
    img: {type: String}
});

const Form = mongoose.model('Form', formSchema);

module.exports = Form;