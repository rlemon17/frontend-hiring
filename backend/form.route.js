const router = require('express').Router();
let Form = require('./form.model');

router.route('/').get((req, res) => {
    Form.find()
        .then(forms => res.json(forms))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
    let date = new Date();
    const newForm = new Form({
        timestamp: date,
        deadline: req.body.deadline,
        goals: req.body.goals,
        firstImp: req.body.firstImp,
        contact: req.body.contact,
        frequency: req.body.frequency,
        medium: req.body.medium,
        budget: req.body.budget,
        img: req.body.img
    });

    newForm.save()
        .then(() => res.json("Form submission successful!"))
        .catch(err => res.status(400).json('Form submission error: ' + err));
});

module.exports = router;