const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const formRouter = require('./form.route');

require('dotenv').config();

const app = express();

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true });

app.use('/forms', formRouter);

app.get("/", (req, res) => res.send("Backend Server for Project Questionnaire running successfully."));
app.listen(3001, () => {
    console.log("Backend server running on port 3001");
})